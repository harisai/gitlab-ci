resources = \
	resources/css/bootstrap.min.css \
	resources/js/d3.v3.min.js \
	resources/css/fa.min.css \
	resources/js/hogan.min.js

resource_packages = \
	libjs-bootstrap \
	libjs-d3 \
	fonts-font-awesome \
	python3-xstatic-hogan

resources: $(resources)

.PHONY: resources

resources/css/bootstrap.min.css: /usr/share/javascript/bootstrap/css/bootstrap.min.css
	ln -sfT $^ $@

resources/js/d3.v3.min.js: /usr/share/javascript/d3/d3.min.js
	ln -sfT $^ $@

# FIXME: should use a proper libjs-hogan package
resources/js/hogan.min.js: /usr/lib/python3/dist-packages/xstatic/pkg/hogan/data/hogan.js
	ln -sfT $^ $@

resources/css/fa.min.js: debian/font-awesome.woff.base64

resources_tmp += debian/font-awesome.woff.base64

debian/font-awesome.woff.base64: /usr/share/fonts-font-awesome/fonts/fontawesome-webfont.woff
	(printf 'src: url(data:application/font-woff;charset=utf-8;base64,' && base64 --wrap=0 $^ && echo ") format('woff');") > $@ || ($(RM) $@; false)

resources_tmp += resources/css/fa.css
resources/css/fa.css: /usr/share/fonts-font-awesome/css/font-awesome.css debian/font-awesome.woff.base64
	   sed -e '/src:/d; /font-family/ r debian/font-awesome.woff.base64' /usr/share/fonts-font-awesome/css/font-awesome.css > $@ || ($(RM) $@; false)

resources/css/fa.min.css: resources/css/fa.css
	cssmin > $@ < $^ ||  ($(RM) $@; false)
